﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class MathTool {

    public static float TargetDistanceToLine (Vector3 direction, Vector3 targetPosition)
    {
        float projectionLength = Vector3.Dot (targetPosition, direction.normalized);
        Vector3 projection = direction.normalized * projectionLength;

        return Vector3.Distance (targetPosition, projection);
    }

    public static bool IsTargetExactInLine (Vector3 direction, Vector3 targetPosition, float exactDeviation = float.NaN)
    {
        float distance = TargetDistanceToLine (direction, targetPosition);

        if (float.IsNaN (exactDeviation))
        {
            return Mathf.Approximately (distance, 0);
        }
        else
        {
            return distance < exactDeviation;
        }
    }

    public static bool IsTargetInFront (Vector3 direction, Vector3 targetPosition)
    {
        float dot = Vector3.Dot (direction, targetPosition);
        return dot > 0;
    }

    public static bool IsTargetOnLeft (Vector3 direction, Vector3 targetPosition)
    {
        Vector3 cross = Vector3.Cross (direction, targetPosition);
        return cross.z > 0;
    }

    public static Vector2 VectorRotation (Vector2 vector, float degree)
    {
        float sx = vector.x;
        float sy = vector.y;
        float radian = degree * Mathf.Deg2Rad;

        float tx = sx * Mathf.Cos (radian) - sy * Mathf.Sin (radian);
        float ty = sx * Mathf.Sin (radian) + sy * Mathf.Cos (radian);

        return new Vector2 (tx, ty);
    }
}
