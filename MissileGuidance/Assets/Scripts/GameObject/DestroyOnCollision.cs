﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyOnCollision : MonoBehaviour {

    [SerializeField] GameObject explosionPrefabs;
    [SerializeField] Destroyer destroyer;

    private void OnTriggerEnter2D (Collider2D collision)
    {
        GameObject explosion = Instantiate (explosionPrefabs);
        explosion.transform.position = transform.position;
        if(destroyer != null)
        {
            destroyer.Destroy ();
        }
        else
        {
            Destroy (gameObject);
        }

    }
}
