﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CausalityGuidance : MonoBehaviour, IGuidance {

    [SerializeField] float guidanceDelay;
    [SerializeField] float guidanceSpeed = float.NaN;
    [SerializeField] float guidanceAngleSpeed = float.PositiveInfinity;
    [SerializeField] bool useDefaultTarget;

    [SerializeField] GameObject causalityParent;
    [Header ("Component")]
    [SerializeField] MoveBySpeed speedControl;


    bool hasInit;
    bool hasOver;
    float startGuidanceTime;
    Transform currentParent;
    Transform target;

    private void Start ()
    {
        startGuidanceTime = Time.time + guidanceDelay;
    }

    private void FixedUpdate ()
    {
        if (Time.time > startGuidanceTime)
        {
            if (!hasInit)
            {
                InitGuiding ();
                hasInit = true;
            }
            Guiding ();

            if (IsOverTarget() && !hasOver)
            {
                GetComponent<DestroyByTime> ().StartTimer ();
                hasOver = true;
            }
        }
    }

    private void InitGuiding ()
    {
        currentParent = Instantiate (causalityParent, transform.position, transform.rotation).transform;
        transform.SetParent (currentParent, true);
        if (useDefaultTarget)
        {
            target = DataManager.defaultTarget;
        }
        if (!float.IsNaN(guidanceSpeed))
        {
            speedControl.speed = guidanceSpeed;
        }
    }

    private void Guiding ()
    {
        Vector2 targetDirection = target.position - currentParent.position;
        Vector2 parentDirection = currentParent.rotation * Vector2.up;

        float angle = Vector2.Angle (targetDirection, parentDirection);
        angle = Mathf.Min (angle, guidanceAngleSpeed * Time.fixedDeltaTime);

        Quaternion rotationQuaternion;
        if (MathTool.IsTargetOnLeft(parentDirection, targetDirection))
        {
            rotationQuaternion = Quaternion.AngleAxis (angle, Vector3.forward);
        }
        else
        {
            rotationQuaternion = Quaternion.AngleAxis (-angle, Vector3.forward);
        }

        currentParent.rotation = rotationQuaternion * currentParent.rotation;
    }

    public void SetGuidanceDelay (float delay)
    {
        guidanceDelay = delay;
    }

    private bool IsOverTarget ()
    {
        float missileDistance = Vector2.Distance (transform.position, currentParent.position);
        float targetDistance = Vector2.Distance (target.position, currentParent.position);
        return missileDistance > targetDistance;
    }
}
