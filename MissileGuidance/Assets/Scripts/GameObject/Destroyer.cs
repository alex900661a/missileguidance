﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Destroyer : MonoBehaviour {

    public GameObject[] delayDestroy;
    public float delayTime;

	public void Destroy()
    {
        foreach (GameObject current in delayDestroy)
        {
            current.transform.SetParent (null, true);
            Destroy (current, delayTime);
        }

        Destroy (gameObject);
    }
}
