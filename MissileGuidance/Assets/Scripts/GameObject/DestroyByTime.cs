﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyByTime : MonoBehaviour {

    public float lifeTime;
    [SerializeField] bool startTimerOnStart;
    [SerializeField] Destroyer destroyer;

    float destroyTime = float.PositiveInfinity;

    private void Start ()
    {
        if (startTimerOnStart)
        {
            StartTimer ();
        }
    }

    public void StartTimer ()
    {
        if (destroyer != null)
        {
            destroyTime = Time.time + lifeTime;
        }
        else
        {
            Destroy (gameObject, lifeTime);
        }
    }

    private void Update () 
    {
        if (Time.time > destroyTime)
        {
            destroyer.Destroy ();
        }
	}
}
