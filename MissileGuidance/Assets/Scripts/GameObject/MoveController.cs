﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveController : MonoBehaviour {

    [SerializeField] GameObject teleportEffectOrigin;
    [SerializeField] GameObject teleportEffectTarget;
    [SerializeField] float speed;
    Rigidbody2D rb;
    bool hasTeleport;

    private void Start ()
    {
        rb = GetComponent<Rigidbody2D> ();
    }

    private void FixedUpdate ()
    {
        CheckTeleport ();
        CheckMove ();
    }

    private void CheckTeleport ()
    {
        if (Input.GetAxis ("Teleport") > 0)
        {
            if (!hasTeleport)
            {
                GameObject currentTp = Instantiate (teleportEffectOrigin);
                currentTp.transform.position = transform.position;

                hasTeleport = true;
                Vector3 teleportTarget = Camera.main.ScreenToWorldPoint (Input.mousePosition);
                teleportTarget.z = 0;
                transform.position = teleportTarget;

                currentTp = Instantiate (teleportEffectTarget);
                currentTp.transform.position = teleportTarget;
            }
        }
        else
        {
            hasTeleport = false;
        }
    }

    public void CheckMove ()
    {
        float x = Input.GetAxis ("Horizontal");
        float y = Input.GetAxis ("Vertical");

        Vector2 speedVector = new Vector2 (x, y);
        if (speedVector.magnitude > 1)
        {
            speedVector = speedVector.normalized;
        }

        rb.velocity = speedVector * speed;
    }

}
