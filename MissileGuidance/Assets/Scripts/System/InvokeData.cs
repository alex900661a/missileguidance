﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InvokeData : MonoBehaviour {

    [SerializeField] DataManager.DataType type;

	void Start () 
    {
        switch (type)
        {
        case DataManager.DataType.defaultTarget:
            DataManager.defaultTarget = transform;
            break;
        }
	}

}
