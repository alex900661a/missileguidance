﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraZoomWithTwoGameObject : MonoBehaviour {

    [SerializeField] Transform target1;
    [SerializeField] Transform target2;
    [SerializeField] float minSize;
    [SerializeField] float maxSize = float.PositiveInfinity;
    [SerializeField] float borderSize;

    Camera cam;
    float posZ;
    bool hasSettingBorderSize;

    private void Start ()
    {
        cam = GetComponent<Camera> ();
        posZ = transform.position.z;
    }

    private void Update () 
    {
        transform.position = (target1.position + target2.position) / 2 + Vector3.forward * posZ;
        float size = Vector2.Distance (target1.position, target2.position) / 2 + borderSize;
        size = Mathf.Max (minSize, size);
        size = Mathf.Min (maxSize, size);
        cam.orthographicSize = size;
        CheckChangeZoom ();
    }

    public void CheckChangeZoom ()
    {
        if (Input.GetAxis ("ZoomOut") > 0)
        {
            if (!hasSettingBorderSize)
            {
                borderSize += 5;
                hasSettingBorderSize = true;
            }
        }
        else if (Input.GetAxis ("ZoomIn") > 0)
        {
            if (!hasSettingBorderSize)
            {
                borderSize -= 5;
                if (borderSize < 0) borderSize = 0;
                hasSettingBorderSize = true;
            }
        }
        else
        {
            hasSettingBorderSize = false;
        }
    }
}
