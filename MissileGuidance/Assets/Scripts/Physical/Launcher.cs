﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Launcher : MonoBehaviour {

    enum MissileMode
    {
        direct,
        rotation,
        gravity,
        side,
        physical,
        causality
    }

    enum LaunchingMode
    {
        volley,
        scattering,
        nova
    }

    [Header ("Base")]
    [SerializeField] GameObject[] missilePrefabs;

    [Header ("Mode")]
    [SerializeField] MissileMode missileMode;
    [SerializeField] LaunchingMode launchingMode;

    [Header ("Attribute")]
    [SerializeField] int numberOfProjection;
    [SerializeField] float fireInterval;
    [SerializeField] float scatteringArea;
    [SerializeField] float guidanceDelay = float.NaN;

    float nextFire;
    private float i;

    public void FixedUpdate ()
    {
        if (Input.GetAxis ("Fire1") > 0)
        {
            if (Time.time > nextFire)
            {
                nextFire = Time.time + fireInterval;

                Launch ();
            }
        }
        else
        {
            nextFire = 0;
        }
    }

    private void Launch()
    {
        if (numberOfProjection > 1)
        {
            switch (launchingMode)
            {
            case LaunchingMode.volley:
                {
                    float projectionDistance = scatteringArea / (numberOfProjection - 1);
                    Vector2 launchingStartPosition = transform.transform.position + (transform.rotation * (Vector2.left * scatteringArea / 2));
                    Vector2 distanceVector = transform.rotation * (Vector2.right * projectionDistance);
                    for (int i = 0; i < numberOfProjection; i++)
                    {
                        LaunchMissile (launchingStartPosition + distanceVector * i, transform.rotation);
                    }
                    break;
                }
            case LaunchingMode.scattering:
                {
                    float prejectionAngle = scatteringArea / (numberOfProjection - 1);
                    Quaternion launchingStartRotation = transform.transform.rotation;
                    launchingStartRotation *= Quaternion.AngleAxis (scatteringArea / 2, Vector3.back);
                    for (int i = 0; i < numberOfProjection; i++)
                    {
                        Quaternion intervalQuaternion = Quaternion.AngleAxis (prejectionAngle * i, Vector3.forward);
                        LaunchMissile (transform.position, launchingStartRotation * intervalQuaternion);
                    }
                    break;
                }
            case LaunchingMode.nova:
                {
                    float prejectionAngle = 360 / (numberOfProjection);
                    Quaternion launchingStartRotation = transform.transform.rotation;
                    for (int i = 0; i < numberOfProjection; i++)
                    {
                        Quaternion intervalQuaternion = Quaternion.AngleAxis (prejectionAngle * i, Vector3.forward);
                        LaunchMissile (transform.position, launchingStartRotation * intervalQuaternion);
                    }
                    break;
                }
            }
        }
        else
        {
            LaunchMissile (transform.position, transform.rotation);
        }
    }

    private void LaunchMissile (Vector2 pos, Quaternion rot)
    {
        GameObject missile = missilePrefabs[(int)missileMode];
        GameObject currentMissile = Instantiate (missile);
        currentMissile.transform.position = pos;
        currentMissile.transform.rotation = rot;
        if (!float.IsNaN (guidanceDelay))
        {
            currentMissile.GetComponent<IGuidance> ().SetGuidanceDelay (guidanceDelay);
        }
    }
}
