﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SideGuidance : Guidance {

    const float IN_LINE_MODE_EEXACT_DEVIATION = 0.01f;

    [SerializeField] float guidanceSpeed;
    [SerializeField] float sideSpeed;
    [SerializeField] bool limitSpeed;

    float limitedForwardSpeed;
    Vector2 forward;
    bool overTarget;
    bool inLineMode;

    protected override void Init ()
    {
        base.Init ();
        limitedForwardSpeed = Mathf.Sqrt (Mathf.Max (guidanceSpeed * guidanceSpeed - sideSpeed * sideSpeed, 0));
    }

    protected override void FirstGuidanceInit ()
    {
        base.FirstGuidanceInit ();
        forward = (rb.velocity.normalized * guidanceSpeed).normalized;
    }

    protected override void Guiding ()
    {
        base.Guiding ();

        if (overTarget)
        {
            return;
        }

        if (MathTool.IsTargetInFront(forward, targetVector))
        {
            float sideDistance = MathTool.TargetDistanceToLine (forward, targetVector);
            if (sideDistance < IN_LINE_MODE_EEXACT_DEVIATION)
            {
                rb.velocity = forward * guidanceSpeed;
            }
            else
            {
                Vector2 sideSpeedDirection;
                if (MathTool.IsTargetOnLeft (forward, targetVector))
                {
                    sideSpeedDirection = MathTool.VectorRotation (forward, 90);
                }
                else
                {
                    sideSpeedDirection = MathTool.VectorRotation (forward, -90);
                }

                float speedRatio = sideDistance  / (sideSpeed * Time.fixedDeltaTime);
                speedRatio = Mathf.Min (speedRatio, 1);

                if (limitSpeed)
                {
                    rb.velocity = (forward * limitedForwardSpeed + sideSpeedDirection * sideSpeed) * speedRatio
                        + forward * guidanceSpeed * (1 - speedRatio);
                }
                else
                {
                    rb.velocity = (forward * guidanceSpeed + sideSpeedDirection * sideSpeed) * speedRatio
                         + forward * guidanceSpeed * (1 - speedRatio);
                }
            }
        }
        else
        {
            overTarget = true;
            GetComponent<DestroyByTime> ().StartTimer ();
        }
    }
}
