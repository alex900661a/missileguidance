﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveBySpeed : MonoBehaviour {

    public float speed;

    void FixedUpdate ()
    {
        transform.position += transform.rotation * Vector2.up * speed * Time.fixedDeltaTime;
	}
}
