﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DirectGuidance : Guidance {

    [SerializeField] float speed;
    [SerializeField] float changeDirectionInterval;

    float nextChangeDirection;

    protected override void Guiding ()
    {
        base.Guiding ();

        if (Time.time > nextChangeDirection)
        {
            nextChangeDirection = Time.time + changeDirectionInterval;
            rb.velocity = speed * targetVector.normalized;
        }
    }
}
