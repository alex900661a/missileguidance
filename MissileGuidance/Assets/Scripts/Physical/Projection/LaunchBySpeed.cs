﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaunchBySpeed : MonoBehaviour {

    public float speed;
    public bool launchOnStart;
    Rigidbody2D rb;

    private void Awake ()
    {
        rb = GetComponent<Rigidbody2D> ();
    }

    private void Start ()
    {
        Launch ();
    }

    public void Launch ()
    {
        rb.velocity = transform.rotation * Vector2.up * speed;
    }


}
