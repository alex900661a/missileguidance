﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeepRotationGuidance : AvoidRotationGuidance {

    [Header ("AvoidRotationGuidance")]
    [SerializeField] float keepRotationTime;
    [SerializeField] float noKeepRotationDistance;

    float nextRotationTime;
    bool turnLeft;

    protected override void Guiding ()
    {
        if (Time.time < nextRotationTime && Vector2.Distance (Vector2.zero, targetVector) > noKeepRotationDistance)
        {
            if (turnLeft)
            {
                rb.velocity = MathTool.VectorRotation (frameInitVecocity, rotationSpeed * Time.fixedDeltaTime);
            }
            else
            {
                rb.velocity = MathTool.VectorRotation (frameInitVecocity, -rotationSpeed * Time.fixedDeltaTime);
            }
        }
        else
        {
            base.Guiding ();
        }
    }

    public override void Rotation (float rotationDegree)
    {
        base.Rotation (rotationDegree);
        nextRotationTime = Time.time + keepRotationTime;
        turnLeft = rotationDegree > 0;
    }
}
