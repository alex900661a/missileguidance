﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FaceSpeed : MonoBehaviour {

    private Rigidbody2D rb;

    private void Start ()
    {
        rb = GetComponent<Rigidbody2D> ();
    }

    private void Update () 
    {
        if (rb.velocity.magnitude > 0)
        {
            transform.rotation = Quaternion.LookRotation (rb.velocity, Vector3.back);
        }
	}
}
