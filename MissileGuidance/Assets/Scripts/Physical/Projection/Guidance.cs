﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Guidance : MonoBehaviour, IGuidance {

    [SerializeField] float guidanceDelay;
    [SerializeField] bool useDefaultTarget;

    protected float startGuidanceTime;
    protected Transform target;
    protected Rigidbody2D rb;

    protected Vector2 frameInitVecocity;
    protected Vector2 targetVector;

    bool hasFirstGuidanceInit;

    private void Start ()
    {
        Init ();
    }

    protected virtual void Init ()
    {
        rb = GetComponent<Rigidbody2D> ();
        startGuidanceTime = Time.time + guidanceDelay;
    }

    private void FixedUpdate ()
    {
        if (Time.time > startGuidanceTime)
        {
            if (!hasFirstGuidanceInit)
            {
                FirstGuidanceInit ();
                hasFirstGuidanceInit = true; 
            }
            if (target == null && useDefaultTarget)
            {
                SetTarget ();
            }
            if (target != null)
            {
                GuidanceInitData ();
                Guiding ();
            }

        }
    }

    protected virtual void FirstGuidanceInit ()
    {

    }

    protected virtual void GuidanceInitData ()
    {
        frameInitVecocity = rb.velocity;
        targetVector = target.position - transform.position;
    }

    protected virtual void Guiding ()
    {

    }

    public void SetTarget (Transform target = null)
    {
        if (target != null)
        {
            this.target = target;
        }
        else
        {
            this.target = DataManager.defaultTarget;
        }
    }

    public void SetGuidanceDelay (float delay)
    {
        guidanceDelay = delay;
    }
}
