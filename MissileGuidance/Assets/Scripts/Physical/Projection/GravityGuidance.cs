﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GravityGuidance : Guidance {

    [SerializeField] float gravityAcceleration;
    [SerializeField] float damping;

    float speedKeepPreFixedUpdate;

    protected override void Init ()
    {
        base.Init ();
        speedKeepPreFixedUpdate = Mathf.Pow (1 - damping, Time.fixedDeltaTime);
    }

    protected override void Guiding ()
    {
        base.Guiding ();

        rb.AddForce (targetVector.normalized * gravityAcceleration * rb.mass, ForceMode2D.Force);
        rb.velocity *= speedKeepPreFixedUpdate;
    }
}
