﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AvoidRotationGuidance : RotationGuidance {

    [Header ("AvoidRotationGuidance")]
    [SerializeField] protected float avoidDistance;

    float loopCircleRadius;
    bool avoidMode;

    protected override void Init ()
    {
        base.Init ();

        float speed = float.IsNaN (guidanceSpeed) ? rb.velocity.magnitude : guidanceSpeed;
        float loopCircleTime = 360 / rotationSpeed;
        float circumference = speed * loopCircleTime;
        loopCircleRadius = circumference / (2 * Mathf.PI);

    }

    protected override void Guiding ()
    {
        if (avoidMode)
        {
            bool onLeft = MathTool.IsTargetOnLeft (frameInitVecocity, targetVector);
            avoidMode = IsTargetInLoopCircle (frameInitVecocity, targetVector, avoidDistance);
        }
        else
        {
            base.Guiding ();
        }
    }

    public override void Rotation (float rotationDegree)
    {
        if (IsTargetInLoopCircle (frameInitVecocity, targetVector))
        {
            avoidMode = true;
        }
        else
        {
            base.Rotation (rotationDegree);
        }
    }

    public bool IsTargetInLoopCircle (Vector2 velocityDirection, Vector2 targetDirection, float additiveRadius = 0)
    {
        Vector2 directionVector;

        if (MathTool.IsTargetOnLeft(velocityDirection, targetDirection))
        {
            directionVector = MathTool.VectorRotation (velocityDirection, 90).normalized;
        }
        else
        {
            directionVector = MathTool.VectorRotation (velocityDirection, -90).normalized;
        }
        Vector2 leftCircleCenter = directionVector * (loopCircleRadius + additiveRadius);
        float targetToCenterDistance = Vector2.Distance (targetDirection, leftCircleCenter);
        return targetToCenterDistance < (loopCircleRadius + additiveRadius);
    }
}
