﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PhysicalGuidance : Guidance {

    [SerializeField] float centripetalAcceleration;
    [SerializeField] float guidanceSpeed = float.NaN;
    [SerializeField] bool sideGuidance;

    Vector2 forward;
    bool overTarget;

    protected override void FirstGuidanceInit ()
    {
        base.FirstGuidanceInit ();
        if (sideGuidance)
        {
            forward = rb.velocity.normalized;
        }
    }

    protected override void GuidanceInitData ()
    {
        base.GuidanceInitData ();
        if (sideGuidance)
        {
            frameInitVecocity = forward;
        }
    }

    protected override void Guiding ()
    {
        base.Guiding ();

        if (overTarget)
        {
            return;
        }

        if (sideGuidance)
        {
            if (!MathTool.IsTargetInFront (frameInitVecocity, targetVector))
            {
                overTarget = true;
                GetComponent<DestroyByTime> ().StartTimer ();
            }
        }

        if (MathTool.IsTargetOnLeft (frameInitVecocity, targetVector))
        {
            Vector2 forceDirection = MathTool.VectorRotation(frameInitVecocity.normalized, 90);
            rb.AddForce (forceDirection * centripetalAcceleration * rb.mass, ForceMode2D.Force);
        }
        else
        {
            Vector2 forceDirection = MathTool.VectorRotation (frameInitVecocity.normalized, -90); 
            rb.AddForce (forceDirection * centripetalAcceleration * rb.mass, ForceMode2D.Force);
        }

        if (!float.IsNaN (guidanceSpeed))
        {
            rb.velocity = rb.velocity.normalized * guidanceSpeed;
        }
    }
}
