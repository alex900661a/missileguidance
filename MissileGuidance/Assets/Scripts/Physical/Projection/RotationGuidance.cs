﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotationGuidance : Guidance {

    [Header("RotationGuidance")]
    [SerializeField] protected float rotationSpeed;
    [SerializeField] protected float guidanceSpeed = float.NaN;

    protected override void FirstGuidanceInit ()
    {
        base.FirstGuidanceInit ();
        if (!float.IsNaN (guidanceSpeed))
        {
            rb.velocity = rb.velocity.normalized * guidanceSpeed;
        }
    }

    protected override void Guiding ()
    {
        base.Guiding ();

        float rotationDegree = Mathf.Min (Vector2.Angle (frameInitVecocity, targetVector), rotationSpeed * Time.fixedDeltaTime);

        if (MathTool.IsTargetOnLeft (frameInitVecocity, targetVector))
        {
            Rotation (rotationDegree);
        }
        else
        {
            Rotation (-rotationDegree);
        }

    }

    public virtual void Rotation (float rotationDegree)
    {
        rb.velocity = MathTool.VectorRotation (frameInitVecocity, rotationDegree);
    }
}
