﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IGuidance {

    void SetGuidanceDelay (float delay);
}
